from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from lifedesc import settings

urlpatterns = [
    path('', include('desc.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)