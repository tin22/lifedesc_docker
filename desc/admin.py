import logging

from django.contrib import admin
from django.contrib import messages
from django.contrib.admin import AdminSite
from django.db.models import Avg

from .models import Daily, Thought, Tag

# from django.contrib.auth.models import User, Group

# admin.site.unregister(User)
# admin.site.unregister(Group)
AdminSite.site_header = 'Редактирование записи ежедневника'
logger = logging.getLogger(__name__)


# AdminSite.enable_nav_sidebar = False


class StepsListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'Пройдено шагов'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'steps'

    def lookups(self, request, model_admin):
        return (
            ('less', 'Менее 10000'),
            ('great', '10000 и более'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'less':
            return queryset.filter(steps__lte=10000)
        if self.value() == 'great':
            return queryset.filter(steps__gte=10000)


class SleeptimeFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'Медленный сон'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'deep_sleeptime'

    def lookups(self, request, model_admin):
        return (
            ('less', 'Менее 2 часов'),
            ('great', '2 часа и более'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'less':
            return queryset.filter(deep_sleeptime__lt='02:00:00')
        if self.value() == 'great':
            return queryset.filter(deep_sleeptime__gte='02:00:00')


class ThoughtInline(admin.TabularInline):
    model = Thought
    ordering = ['timestamp']
    extra = 0


@admin.register(Daily)
class DailyAdmin(admin.ModelAdmin):
    actions = ['average_steps']

    list_display = (
        'date', 'weight', 'bmi', 'push_ups', 'squates', 'steps', 'sleeptime', 'deep_sleeptime', 'dss', 'temperature',
        'sa',)
    list_filter = ('date', StepsListFilter, SleeptimeFilter)
    ordering = ('-date',)

    inlines = [ThoughtInline]

    def average_steps(self, request, queryset):
        avg_steps = int(queryset.aggregate(Avg('steps'))['steps__avg'])
        messages.info(request, 'В среднем вы проходили  по ' +
                      str(avg_steps) + ' шагов в день')
        # self.message_user(request, "Action performed.")  # Так наверное правильнее

    average_steps.short_description = "Среднее число шагов"


@admin.register(Thought)
class ThoughtAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'record',)
    ordering = ['-timestamp', ]
    list_filter = ('tags',)
    search_fields = ('record',)
    date_hierarchy = 'timestamp'


@admin.register(Tag)
class TagsAdmin(admin.ModelAdmin):
    list_display = ['tag', ]
# admin.site.register(Thought)
