from django.urls import path

from . import views

app_name = "desc"

urlpatterns = [
    path('', views.DailyLastListView.as_view(), name='daily_list'),
    path('day/<int:pk>/', views.DailyDetailView.as_view(), name='daily_detail'),
    # path('thoughts/feed/', Last_thoughts(), name='thoughts_feed'),
    path('thoughts/search/', views.ThoughtsSearch.as_view(), name='search'),
    path('daily/<str:date>/', views.DailyDetail.as_view(), name='detail'),
    path('last/', views.DailyLastViewSet.as_view(), name='last'),
    path('alltags/', views.AllTagsList.as_view(), name='all_tags')
    # path('thoughts/', views.ThoughtListView.as_view(), name='thoughts'),
    # path('update/<str:date>/', views.ThoughtDetailView.as_view(), name='thought_update'),

]
