from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.urls import reverse

from .models import Daily


class CustomUserTests(TestCase):
    def test_create_user(self):
        """
        Первый тест (совершенно бесполезный).
        Идея в том, чтобы просто создать нового пользователя.
        """
        User = get_user_model()
        user = User.objects.create_user(
            username='will',
            email='will@email.com',
            password='testpass123'
        )
        self.assertEqual(user.username, 'will')
        self.assertEqual(user.email, 'will@email.com')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)


class DailyTests(TestCase):
    def setUp(self):
        self.daily = Daily.objects.create(
            date='2020-07-15',
            push_ups=20,
            squates=10,
            weight=77.7,
            steps=8245,
            sleeptime='07:47:00',
            deep_sleeptime='01:40:00',
            temperature=36.5
        )

    def test_daily_represetation(self):
        self.assertEqual(str(self.daily), '2020-07-15 77.7 (22.7) 20 10 8245 07:47:00 01:40:00 36.5')

    def test_daily_create_almost_empty_value(self):
        daily = Daily.objects.create(date='2020-07-14')
        self.assertEqual(daily.date, '2020-07-14')
        self.assertEqual(daily.push_ups, 0)
        self.assertEqual(daily.squates, 0)
        self.assertEqual(daily.weight, 0.0)
        self.assertEqual(daily.steps, 0)
        self.assertEqual(daily.sleeptime, '00:00:00')
        self.assertEqual(daily.deep_sleeptime, '00:00:00')
        self.assertEqual(daily.temperature, 35.0)
