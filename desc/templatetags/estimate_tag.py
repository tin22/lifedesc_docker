import logging
from django import template

register = template.Library()
logger = logging.getLogger(__name__)


@register.filter(name='to_float')
def to_float(val: str) -> float:
    if not(val[-1].isdigit()): val = val[:-1]
    return float(val)
