import logging

from datetime import datetime

import requests
from django import template

logger = logging.getLogger(__name__)
register = template.Library()


@register.filter(name='isdayoff')
def isdayoff(date: str) -> str:
    """
    INPUT: desired_date <str>
    OUTPUT: 'Рабочий' | 'Выходной' <str>
    """
    try:
        h = requests.get('https://isdayoff.ru/' + datetime.strftime(date, format('%Y%m%d'))).json()
    except Exception:
        logger.warning("https://isdayoff.ru/ is not available.")
        return ''
    return '(Выходной)' if h == 1 else '(Рабочий)'
