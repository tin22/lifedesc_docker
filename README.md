Lifedesc
========

Серверная часть *lifedesc* переписанная заново с целью избавится от legacy кода. И подготовиться к переходу на Django 4.

Поставляет API для работы с базой данных, Django-вскую админку и простейшую визуализацию.

