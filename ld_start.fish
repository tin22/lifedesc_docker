#!/usr/bin/env fish

source /home/tin/.cache/pypoetry/virtualenvs/lifedesc-oQCKpwNr-py3.10/bin/activate.fish

gunicorn --bind 0.0.0.0:8000 lifedesc.wsgi | source
